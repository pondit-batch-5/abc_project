import 'dart:math';

import 'package:flutter/material.dart';

class DicePage extends StatefulWidget {
  const DicePage({super.key});

  @override
  State<DicePage> createState() => _DicePageState();
}

class _DicePageState extends State<DicePage> {
  int diceNumber = 2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(32.0),
              child: Row(
                children: [
                  Flexible(
                    child: Image.asset(
                      'assets/images/dice_$diceNumber.png',
                    ),
                  ),
                  SizedBox(width: 16,),
                  Flexible(
                    child: Image.asset(
                      'assets/images/dice_$diceNumber.png',
                    ),
                  ),
                ],
              ),
            ),
            TextButton(
              onPressed: () {
                diceNumber = Random().nextInt(6) + 1;
                print(diceNumber);
                setState(() {});
              },
              child: Text('Spin'),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      Text('Your Name'),
                      Text('12'),
                    ],
                  ),
                  Column(
                    children: [
                      Text('Opponent'),
                      Text('2'),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
