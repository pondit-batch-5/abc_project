import 'package:abc_project/dice_page.dart';
import 'package:flutter/material.dart';
import 'login_page.dart';
void main() {
  runApp(MyAppPage());
}

class MyAppPage extends StatelessWidget {
  const MyAppPage({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.light(),
      debugShowCheckedModeBanner: false,
      home: DicePage(),
    );
  }
}
